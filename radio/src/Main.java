public class Main {

    public static void main(String[] args) {

        Radio app = new Radio(true, 2, 98.7);
        app.setFrequenz(78.9);
        app.aus();
        app.leiser();
        app.an();
        app.lauter();
        app.lauter();

        System.out.printf(app.toString());
    }
}
