public class Radio {

    private boolean istAn;
    private boolean eingeschaltet= false;
    private int lautstaerke = 0;
    private double frequenz = 0;


    public Radio(boolean istAn, int lautstaerke, double frequenz) {
        this.istAn = istAn;
        this.lautstaerke = lautstaerke;
        this.frequenz = frequenz;
    }

    public Radio() {

    }

    public void an (){
        if(eingeschaltet == false){
            eingeschaltet = true;
        }
    }

    public void aus (){
        if(eingeschaltet == true){
            eingeschaltet = false;
        }
    }

    public void lauter(){
        if(eingeschaltet == true){
            lautstaerke++;
        }
    }

    public void leiser() {
        if (eingeschaltet == true && lautstaerke > 0){
            lautstaerke--;
        }
    }

    public boolean isEingeschaltet() {

        return eingeschaltet;
    }

    public void setEingeschaltet(boolean eingeschaltet) {
        this.eingeschaltet = eingeschaltet;
    }

    public boolean isIstAn() {
        return istAn;
    }

    public void setIstAn() {
        if (eingeschaltet == false){
            eingeschaltet =  true;
        }
    }

    public int getLautstaerke() {
        return lautstaerke;
    }

    public void setLautstaerke(int lautstaerke) {
        this.lautstaerke = lautstaerke;
    }

    public double getFrequenz() {
        return frequenz;
    }

    public void setFrequenz(double frequenz) {
        this.frequenz = frequenz;
    }

    @Override
    public String toString() {
        return "Radio an:" + " frequenz = " + frequenz +
                ", lautstaerke = " + lautstaerke;
    }

    /*frequenz soll zwwischen 85.0 und 110

    *
    * lauter() ++ ||--  leiser ()
    *
    *
    *
     */
}
